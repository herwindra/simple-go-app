# Use the golang image for the build stage
FROM golang:alpine AS builder

# Set environment variables
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64

# Create a working directory
WORKDIR /app

# Copy go.mod and go.sum
COPY go.mod go.sum ./

# Download dependencies
RUN go mod download

# Copy the source code
COPY . .

# Build the application
RUN go build -o simple-go-app .

# Use a minimal image to run the application
FROM alpine

# Install CA certificates to enable HTTPS
RUN apk add --no-cache ca-certificates

# Copy the application from the build stage
COPY --from=builder /app/simple-go-app /simple-go-app
COPY --from=builder /app/static /static

# Expose port 8080 for the HTTP server
EXPOSE 8080

# Set the command to run the application
CMD ["/simple-go-app"]
