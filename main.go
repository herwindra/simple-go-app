package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(2)

	// Start the HTTP file server
	go func() {
		defer wg.Done()
		http.Handle("/", http.FileServer(http.Dir("./static")))
		fmt.Println("Server running at http://localhost:8080")
		if err := http.ListenAndServe(":8080", nil); err != nil {
			log.Fatalf("Failed to start HTTP server: %v", err)
		}
	}()

	// Start the Telegram bot
	go func() {
		defer wg.Done()
		// Replace "YOUR_TELEGRAM_BOT_TOKEN" with your Telegram bot token
		bot, err := tgbotapi.NewBotAPI("7418729844:AAFIwco8oPrXZ_htHmc4_cEnNx4SnUj3csM")
		if err != nil {
			log.Panic(err)
		}

		bot.Debug = true

		log.Printf("Authorized on account %s", bot.Self.UserName)

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates := bot.GetUpdatesChan(u)

		for update := range updates {
			if update.Message != nil { // If there's a message
				log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

				var msg tgbotapi.MessageConfig

				switch update.Message.Text {
				case "hello":
					msg = tgbotapi.NewMessage(update.Message.Chat.ID, "Hi there!")
				case "how are you":
					msg = tgbotapi.NewMessage(update.Message.Chat.ID, "I'm just a bot, but I'm doing great!")
				case "what is your name":
					msg = tgbotapi.NewMessage(update.Message.Chat.ID, "I am a simple bot created in Go.")
				default:
					msg = tgbotapi.NewMessage(update.Message.Chat.ID, "I don't understand that. Try saying 'hello', 'how are you', or 'what is your name'.")
				}

				bot.Send(msg)
			}
		}
	}()

	wg.Wait()
}
